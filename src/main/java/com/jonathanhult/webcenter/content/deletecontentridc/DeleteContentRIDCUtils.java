package com.jonathanhult.webcenter.content.deletecontentridc;

import static com.jonathanhult.webcenter.content.deletecontentridc.DeleteContentRIDCConstants.COLON;
import static com.jonathanhult.webcenter.content.deletecontentridc.DeleteContentRIDCConstants.DATASOURCE;
import static com.jonathanhult.webcenter.content.deletecontentridc.DeleteContentRIDCConstants.DELETE_DOC;
import static com.jonathanhult.webcenter.content.deletecontentridc.DeleteContentRIDCConstants.DOCUMENTS;
import static com.jonathanhult.webcenter.content.deletecontentridc.DeleteContentRIDCConstants.DOT_CFG;
import static com.jonathanhult.webcenter.content.deletecontentridc.DeleteContentRIDCConstants.D_DOC_NAME;
import static com.jonathanhult.webcenter.content.deletecontentridc.DeleteContentRIDCConstants.D_ID;
import static com.jonathanhult.webcenter.content.deletecontentridc.DeleteContentRIDCConstants.D_REV_CLASS_ID_EQUALS;
import static com.jonathanhult.webcenter.content.deletecontentridc.DeleteContentRIDCConstants.GET_DATARESULTSET;
import static com.jonathanhult.webcenter.content.deletecontentridc.DeleteContentRIDCConstants.IDC_SERVICE;
import static com.jonathanhult.webcenter.content.deletecontentridc.DeleteContentRIDCConstants.ORDER_BY_D_ID;
import static com.jonathanhult.webcenter.content.deletecontentridc.DeleteContentRIDCConstants.ORDER_CLAUSE;
import static com.jonathanhult.webcenter.content.deletecontentridc.DeleteContentRIDCConstants.PORT;
import static com.jonathanhult.webcenter.content.deletecontentridc.DeleteContentRIDCConstants.PROTOCOL;
import static com.jonathanhult.webcenter.content.deletecontentridc.DeleteContentRIDCConstants.RESULT_NAME;
import static com.jonathanhult.webcenter.content.deletecontentridc.DeleteContentRIDCConstants.RIDC;
import static com.jonathanhult.webcenter.content.deletecontentridc.DeleteContentRIDCConstants.SERVER;
import static com.jonathanhult.webcenter.content.deletecontentridc.DeleteContentRIDCConstants.SYSADMIN;
import static com.jonathanhult.webcenter.content.deletecontentridc.DeleteContentRIDCConstants.WHERE_CLAUSE;
import static org.slf4j.ext.XLoggerFactory.getXLogger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import oracle.stellent.ridc.IdcClientException;
import oracle.stellent.ridc.IdcClientManager;
import oracle.stellent.ridc.IdcContext;
import oracle.stellent.ridc.model.DataBinder;
import oracle.stellent.ridc.model.DataObject;
import oracle.stellent.ridc.model.impl.DataBinderImpl;
import oracle.stellent.ridc.protocol.ServiceException;
import oracle.stellent.ridc.protocol.ServiceResponse;
import oracle.stellent.ridc.protocol.intradoc.IntradocClient;

import org.slf4j.ext.XLogger;

/**
 * @author Jonathan Hult
 */
public class DeleteContentRIDCUtils {

	private static final XLogger logger = getXLogger(DeleteContentRIDCUtils.class);

	// RIDC utilities
	private static IdcClientManager idcClientManager;
	private static IntradocClient idcClient;

	private static Map<String, Map<String, String>> configVariableNames;

	/**
	 * Deletes a content item based on dRevClassID.
	 * 
	 * @param dRevClassID
	 *            The content item to delete.
	 * @return true if the content item was successfully deleted; otherwise,
	 *         false.
	 */
	protected static boolean deleteContent(final String dRevClassID) {
		logger.entry(dRevClassID);

		// Was delete successful?
		boolean isSuccess = false;

		try {
			final DataBinder serviceBinder = new DataBinderImpl();

			// Get metadata for this content item (dRevClassID)
			final List<DataObject> docInfo = getDataResultSet(DOCUMENTS, D_REV_CLASS_ID_EQUALS + dRevClassID, ORDER_BY_D_ID);

			try {
				serviceBinder.putLocal(IDC_SERVICE, DELETE_DOC);
				serviceBinder.putLocal(D_ID, docInfo.get(0).get(D_ID));
				serviceBinder.putLocal(D_DOC_NAME, docInfo.get(0).get(D_DOC_NAME));
				// Delete content item
				executeService(serviceBinder, SYSADMIN);
				isSuccess = true;
			} catch (final IdcClientException e) {
				logger.error("Could not delete dRevClassID: " + dRevClassID, e);
			}
		} catch (final IdcClientException e) {
			logger.error("Could not retrieve metadata for dRevClassID: " + dRevClassID, e);
		} finally {
			logger.exit();
		}
		return isSuccess;
	}

	/**
	 * Execute a service.
	 * 
	 * @param binder
	 *            DataBinder containing service parameters.
	 * @param userName
	 *            User to execute service as.
	 * @return DataBinder from service call
	 * @throws IdcClientException
	 */
	protected static DataBinder executeService(DataBinder binder, final String userName) throws IdcClientException {
		logger.entry(binder, userName);

		try {
			logger.debug("Service binder before executing:");
			logger.debug(binder.getLocalData().toString());

			// Execute the request
			final ServiceResponse response = getIntradocClient().sendRequest(new IdcContext(userName), binder);

			// Get the response as a DataBinder
			binder = response.getResponseAsBinder();
			logger.debug("Successfully called service");

			logger.debug("Service binder after executing:");
			logger.debug(binder.getLocalData().toString());

			// Return the response as a DataBinder
			return binder;
		} finally {
			logger.exit();
		}
	}

	/**
	 * Retrieve value from config file.
	 * 
	 * @param configFileName
	 *            The name of the config file.
	 * @return Map containing key/value pairs.
	 */
	protected static Map<String, String> getConfigValues(final String configFileName) {
		logger.entry(configFileName);

		try {

			if (configVariableNames == null) {
				configVariableNames = new HashMap<String, Map<String, String>>();
			}

			if (configVariableNames.get(configFileName) == null) {
				logger.info("Need to retrieve config values for: " + configFileName);

				// Get keys and values
				final Properties props = new Properties();
				try {
					props.load(new FileInputStream(configFileName));
				} catch (final IOException e) {
					logger.error("Unable to retrieve properties from " + configFileName + " file", e);
				}

				@SuppressWarnings({ "unchecked", "rawtypes" })
				final Map<String, String> configs = new HashMap<String, String>((Map) props);
				configVariableNames.put(configFileName, configs);
			}
			logger.trace("Configs: " + configVariableNames.get(configFileName).toString());
			return configVariableNames.get(configFileName);
		} finally {
			logger.exit();
		}
	}

	/**
	 * Get information using service GET_DATARESULTSET.
	 * 
	 * @param dataSource
	 *            The data source to use.
	 * @param whereClause
	 *            The where clause.
	 * @param orderClause
	 *            The order by clause.
	 * @return DataResultSet containing search results.
	 * @throws ServiceException
	 * @throws IdcClientException
	 */
	protected static List<DataObject> getDataResultSet(final String dataSource, final String whereClause, final String orderClause) throws IdcClientException {
		logger.entry(dataSource, whereClause, orderClause);

		List<DataObject> results = new ArrayList<DataObject>();
		try {
			DataBinder serviceBinder = new DataBinderImpl();
			serviceBinder.putLocal(IDC_SERVICE, GET_DATARESULTSET);
			serviceBinder.putLocal(DATASOURCE, dataSource);
			serviceBinder.putLocal(RESULT_NAME, dataSource);

			if (whereClause != null) {
				serviceBinder.putLocal(WHERE_CLAUSE, whereClause);
			}

			if (orderClause != null) {
				serviceBinder.putLocal(ORDER_CLAUSE, ORDER_BY_D_ID);
			}

			serviceBinder = executeService(serviceBinder, SYSADMIN);
			results = serviceBinder.getResultSet(dataSource).getRows();

			logger.debug("Total number of items found: " + results.size());
		} finally {
			logger.exit();
		}
		return results;
	}

	/**
	 * Get IdcClientManager.
	 * 
	 * @return IdcClientManager
	 */
	protected static IdcClientManager getIdcClientManager() {
		logger.entry();

		try {
			if (idcClientManager == null) {
				// Need to create IntradocClient
				idcClientManager = new IdcClientManager();
			}
			return idcClientManager;
		} finally {
			logger.exit();
		}
	}

	/**
	 * Get IntradocClient.
	 * 
	 * @return IntradocClient
	 * @throws IdcClientException
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	protected static IntradocClient getIntradocClient() throws IdcClientException {
		logger.entry();

		try {
			if (idcClient == null) {
				// Get RIDC properties
				final Map<String, String> props = getConfigValues(RIDC + DOT_CFG);

				if (props == null) {
					throw new IllegalArgumentException("Valid RIDC connection parameters were not found");
				}

				// Client to talk to WebCenter Content
				idcClient = (IntradocClient) getIdcClientManager().createClient(props.get(PROTOCOL) + props.get(SERVER) + COLON + props.get(PORT));
				idcClient.getConfig().setSocketTimeout(1000000);
			}
			return idcClient;
		} finally {
			logger.exit();
		}
	}
}
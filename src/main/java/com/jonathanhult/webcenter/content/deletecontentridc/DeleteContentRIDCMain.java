package com.jonathanhult.webcenter.content.deletecontentridc;

import static com.jonathanhult.webcenter.content.deletecontentridc.DeleteContentRIDCConstants.D_REV_CLASS_ID;
import static com.jonathanhult.webcenter.content.deletecontentridc.DeleteContentRIDCConstants.REVISION_IDS;
import static com.jonathanhult.webcenter.content.deletecontentridc.DeleteContentRIDCUtils.getDataResultSet;
import static org.slf4j.ext.XLoggerFactory.getXLogger;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import oracle.stellent.ridc.IdcClientException;
import oracle.stellent.ridc.model.DataObject;

import org.slf4j.ext.XLogger;

/**
 * @author Jonathan Hult
 */
public class DeleteContentRIDCMain {

	private static final XLogger logger = getXLogger(DeleteContentRIDCMain.class);

	public static void main(final String[] args) throws IdcClientException {
		logger.entry();

		try {
			final ExecutorService executor = Executors.newFixedThreadPool(10);

			// Get list of all content (dRevClassIDs) in the system
			final List<DataObject> allContent = getDataResultSet(REVISION_IDS, null, null);
			logger.info("Total number of content items found which are to be deleted: " + allContent.size());

			// Loop through each content item
			for (final DataObject contentItem : allContent) {
				// Represents a content item (similar to dDocName)
				final String dRevClassID = contentItem.get(D_REV_CLASS_ID);
				final Runnable worker = new DeleteContentRIDCRunnable(dRevClassID);
				executor.execute(worker);
			}

			// This will make the executor accept no new threads
			// and finish all existing threads in the queue
			executor.shutdown();
			// Wait until all threads are finish
			while (!executor.isTerminated()) {
			}
		} finally {
			logger.exit();
		}
	}
}

package com.jonathanhult.webcenter.content.deletecontentridc;

import static com.jonathanhult.webcenter.content.deletecontentridc.DeleteContentRIDCUtils.deleteContent;
import static org.slf4j.ext.XLoggerFactory.getXLogger;

import org.slf4j.ext.XLogger;

/**
 * @author Jonathan Hult
 */
public class DeleteContentRIDCRunnable implements Runnable {

	private static final XLogger logger = getXLogger(DeleteContentRIDCRunnable.class);

	private final String dRevClassID;

	protected DeleteContentRIDCRunnable(final String dRevClassID) {
		this.dRevClassID = dRevClassID;
	}

	public void run() {
		logger.entry(dRevClassID);
		try {
			deleteContent(dRevClassID);
		} finally {
			logger.exit();
		}
	}
}
package com.jonathanhult.webcenter.content.deletecontentridc;

/**
 * @author Jonathan Hult
 */
public class DeleteContentRIDCConstants {
	// Metadata
	protected static final String D_ID = "dID";
	protected static final String D_REV_CLASS_ID = "dRevClassID";
	protected static final String D_DOC_NAME = "dDocName";

	// WebCenter Content
	protected static final String IDC_SERVICE = "IdcService";
	protected static final String SYSADMIN = "sysadmin";

	// Services
	protected static final String DELETE_DOC = "DELETE_DOC";

	// DataResultSet
	protected static final String GET_DATARESULTSET = "GET_DATARESULTSET";
	protected static final String DATASOURCE = "dataSource";
	protected static final String RESULT_NAME = "resultName";
	protected static final String REVISION_IDS = "RevisionIDs";
	protected static final String DOCUMENTS = "Documents";
	protected static final String WHERE_CLAUSE = "whereClause";
	protected static final String D_REV_CLASS_ID_EQUALS = "Revisions.dRevClassID=";
	protected static final String ORDER_CLAUSE = "orderClause";
	protected static final String ORDER_BY_D_ID = " order by Revisions.dRevRank desc";

	// RIDC
	protected static final String PROTOCOL = "protocol";
	protected static final String SERVER = "server";
	protected static final String PORT = "port";
	protected static final String RIDC = "ridc";
	protected static final String DOT_CFG = ".cfg";

	// Misc
	protected static final String COLON = ":";
}
